<?php
/**
 * 2007-2019 PrestaShop SA and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class PickitGlobalModel extends ObjectModel
{
	public $testing_mode;
	public $apikey_webapp;
	public $token_id;
	public $url_webservice;
	public $url_webservice_test;
	public $product_weight;
	public $imposition_available;
	public $estado_inicial;
	//public $ship_type;
	//public $ship_price_opt_dom;
	//public $ship_price_fijo_dom;
	//public $ship_price_porcentual_dom;
	public $ship_price_opt_punto;
	public $ship_price_fijo_punto;
	public $ship_price_porcentual_punto;
	public static $definition = array(
		'table' => _DB_PREFIX_ . 'pickit_global',
		'primary' => 'pickit_id',
        'multilang' => false,
		'fields' => array(
			'pickit_id' 						=>	array('type' => self::TYPE_INT),
			'pickit_testing_mode' 				=> 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'pickit_apikey_webapp' 				=> 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
			'pickit_token_id' 					=> 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
			'pickit_url_webservice' 			=> 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
			'pickit_url_webservice_test' 		=> 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false, 'size' => 255),
			'pickit_product_weight' 			=> 	array('type' => self::TYPE_INT),
			'pickit_product_dim' 				=> 	array('type' => self::TYPE_INT),
			'pickit_imposition_available' 		=> 	array('type' => self::TYPE_INT),
			'pickit_estado_inicial' 			=> 	array('type' => self::TYPE_INT),
			//'pickit_ship_type' 				=> 	array('type' => self::TYPE_INT),
			//'pickit_ship_price_opt_dom' 		=> 	array('type' => self::TYPE_INT),
			//'pickit_ship_price_fijo_dom' 		=> 	array('type' => self::TYPE_INT),
			//'pickit_ship_price_porcentual_dom'=> 	array('type' => self::TYPE_FLOAT),
			'pickit_ship_price_opt_punto' 		=> 	array('type' => self::TYPE_INT),
			'pickit_ship_price_fijo_punto' 		=> 	array('type' => self::TYPE_INT),
			'pickit_ship_price_porcentual_punto'=> 	array('type' => self::TYPE_FLOAT),
		),
	);

	public static function saveGlobalData($testingmode, $apikeyw, $tokenid, $urlws,	$urlwst, $weight, $dim,
	$impost, $estadoInicial, /*$shiptype, $shipdomopt, $shipdomfijo, $shipdomporc,*/ $shippuntoopt, $shippuntofijo, $shippuntoporc,
	$paisTienda, $apikwTest, $tokenidTest, $plus, $servicioDropOff){

		// $sql[] = 'DELETE FROM `' . _DB_PREFIX_ . 'pickit_global`';
		
		/*
		$sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'pickit_global` (
			pickit_testing_mode, pickit_apikey_webapp, 
			pickit_token_id, pickit_url_webservice, pickit_url_webservice_test, pickit_product_weight, 
			pickit_imposition_available, pickit_estado_inicial, pickit_ship_type,
			pickit_ship_price_opt_dom, pickit_ship_price_fijo_dom, pickit_ship_price_porcentual_dom,
			pickit_ship_price_opt_punto, pickit_ship_price_fijo_punto, pickit_ship_price_porcentual_punto) VALUES (
				"'.$testingmode.'", "'.$apikeyw.'", "'.$tokenid.'", "'.$urlws.'", "'.$urlwst.'",
				"'.$weight.'", "'.$impost.'", "'.$estadoInicial.'", "'.$shiptype.'", "'.$shipdomopt.'", 
				"'.$shipdomfijo.'", "'.$shipdomporc.'", "'.$shippuntoopt.'", "'.$shippuntofijo.'", "'.$shippuntoporc.'")';
		*/

		// $sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'pickit_global` (
		// 	pickit_testing_mode, pickit_apikey_webapp, 
		// 	pickit_token_id, pickit_url_webservice, pickit_url_webservice_test, pickit_product_weight, pickit_product_dim,
		// 	pickit_imposition_available, pickit_estado_inicial, pickit_ship_price_opt_punto, pickit_ship_price_fijo_punto, pickit_ship_price_porcentual_punto) VALUES (
		// 		"'.$testingmode.'", "'.$apikeyw.'", "'.$tokenid.'", "'.$urlws.'", "'.$urlwst.'",
		// 		"'.$weight.'", "'.$dim.'", "'.$impost.'", "'.$estadoInicial.'", "'.$shippuntoopt.'", "'.$shippuntofijo.'", "'.$shippuntoporc.'")';

		// foreach ($sql as $query) {
		// 	if (Db::getInstance()->execute($query) == false) {
		// 		return false;
		// 	}
		// }

		$sql = 'TRUNCATE TABLE `' . _DB_PREFIX_ . 'pickit_global`';
		Db::getInstance()->execute($sql);	

		$insertData = array(
			'pickit_testing_mode'  				=>	$testingmode,
			'pickit_apikey_webapp'  			=>	$apikeyw,
			'pickit_token_id'   				=>	$tokenid,
			'pickit_apikey_webapp_test'			=>	$apikwTest,
			'pickit_token_id_test'				=>	$tokenidTest,
			'pickit_pais_tienda'    			=>  $paisTienda,
			'pickit_url_webservice'  			=>  $urlws,
			'pickit_url_webservice_test' 		=>  $urlwst,
			'pickit_product_weight' 			=>  $weight,
			'pickit_product_dim' 				=>  $dim,
			'pickit_imposition_available' 		=>  $impost,
			'pickit_estado_inicial' 			=>  $estadoInicial,
			'pickit_ship_price_opt_punto' 		=>  $shippuntoopt,
			'pickit_ship_price_fijo_punto' 		=>  $shippuntofijo,
			'pickit_ship_price_porcentual_punto'=>  $shippuntoporc,
			'pickit_ship_price_plus_punto'		=>	$plus,
			'pickit_servicio_drop_off'			=>	$servicioDropOff,
			);
	
			//var_dump($insertData); exit;

		Db::getInstance()->insert('pickit_global', $insertData);
	}

	public static function getModoTestSelect (){
			$sql = new DbQuery();
			$sql->select('pickit_testing_mode');
			$sql->from('pickit_global');
			$pickit_testing_mode = Db::getInstance()->executeS($sql)[0]['pickit_testing_mode'];
	
			return $pickit_testing_mode;
	}

}
