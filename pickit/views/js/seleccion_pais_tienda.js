$(document).ready(function () {

    var opt = jQuery("#pickit_pais_tienda").val();
    $("#pickit_pais_tienda").on('change', function (e) {
        opt = jQuery("#pickit_pais_tienda").val();

        switch (opt) {
            case '0':
                $('input[name="pickit_url_webservice"]').val("https://api.pickit.net");
                $('input[name="pickit_url_webservice_test"]').val("https://api.pickitlabs.com");
                break;
            case '1':
                $('input[name="pickit_url_webservice"]').val("https://api.pickit.com.uy/index.php");
                $('input[name="pickit_url_webservice_test"]').val("https://api.pickitlabs.com");
                break;
            case '2':
                $('input[name="pickit_url_webservice"]').val("https://api.pickit.com.mx/index.php");
                $('input[name="pickit_url_webservice_test"]').val("https://api.pickitlabs.com");
                break;
            case '3':
                $('input[name="pickit_url_webservice"]').val("https://api.pickit.com.co/index.php");
                $('input[name="pickit_url_webservice_test"]').val("https://api.pickitlabs.com");
                break;
        }
    });
});