<?php
/**
 * Plugin Name: Pickit
 * Description: Módulo de Pickit para WooCommerce.
 * Version: 1.0.0
 * Author: VDFactory
 *
 * @package Pickit
 */

/**
 * Pickit Functions Class
 */
class PickitUtilities {

    // Devuelve el tipo de envío disponible de la base de datos.
    public function obtenerTipoDeEnvioDisponible() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_type');
        $sql->from('pickit_global');
        $pickit_ship_type = Db::getInstance()->executeS($sql)[0]['pickit_ship_type'];

        return $pickit_ship_type;
    }

    // Devuelve el TokenId de la base de datos.
    public function obtenerTokenId() {
        /**
         * Adaptacion del metodo para los nuevos campos
         * Existe diferencia de tokenId: Produccion y Test
         */
        //Obtener Modo test
        $pickit_testing_mode = self::getModoTestSelec();
        
        /**
         * Modo test => 0 - Habilitado.
         * Modo Test => 1 - Deshabilitado.
         */
        $sql = new DbQuery();
        $sql->from('pickit_global');

        if ( $pickit_testing_mode == 0 ) {
            $sql->select('pickit_token_id_test');
            $pickit_token_id = Db::getInstance()->executeS($sql)[0]['pickit_token_id_test'];
        } else {
            $sql->select('pickit_token_id');
            $pickit_token_id = Db::getInstance()->executeS($sql)[0]['pickit_token_id'];
        }

        return $pickit_token_id;
    }

    public static function getModoTestSelec(){
        $sql = new DbQuery();
        $sql->select('pickit_testing_mode');
        $sql->from('pickit_global');
        $pickit_testing_mode = Db::getInstance()->executeS($sql)[0]['pickit_testing_mode'];

        return $pickit_testing_mode;
    }

    // Devuelve la Apikey del WebApp de la base de datos.
    public function obtenerApikey() {

        $pickit_testing_mode = self::getModoTestSelec();

        $sql = new DbQuery();
        $sql->from('pickit_global');

        /**
         * Modo test => 0 - Habilitado.
         * Modo Test => 1 - Deshabilitado.
         */
        if ( $pickit_testing_mode == 0 ) {
            $sql->select('pickit_apikey_webapp_test');
            $pickit_apikey= Db::getInstance()->executeS($sql)[0]['pickit_apikey_webapp_test'];
        } else {
            $sql->select('pickit_apikey_webapp');
            $pickit_apikey = Db::getInstance()->executeS($sql)[0]['pickit_apikey_webapp'];
        }

        return $pickit_apikey;
    }

    // Devuelve la URL del Web Service de la base de datos.
    // Depende del valor de pickit_testing_mode
    // (Si el modo de testeo está habilitado, devuelve la URL de testeo.)
    public function obtenerUrlWS() {
        $sql = new DbQuery();
        $sql->select('pickit_testing_mode');
        $sql->from('pickit_global');
        $pickit_testing_mode = Db::getInstance()->executeS($sql)[0]['pickit_testing_mode'];

        if( $pickit_testing_mode == 0 ){

            $sql = new DbQuery();
            $sql->select('pickit_url_webservice_test');
            $sql->from('pickit_global');
            $pickit_url_webservice_test = Db::getInstance()->executeS($sql)[0]['pickit_url_webservice_test'];

            return $pickit_url_webservice_test;
        } else {
            
            $sql = new DbQuery();
            $sql->select('pickit_url_webservice');
            $sql->from('pickit_global');
            $pickit_url_webservice = Db::getInstance()->executeS($sql)[0]['pickit_url_webservice'];

            return $pickit_url_webservice;
        }
    }

    // Devuelve el tipo de Precio de envío a domicilio de la base de datos.
    public function obtenerTipoPrecioEnvioDom() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_opt_dom');
        $sql->from('pickit_global');
        $pickit_ship_price_opt_dom = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_opt_dom'];

        return $pickit_ship_price_opt_dom;
    }

    // Devuelve el Precio Fijo de envío a domicilio de la base de datos.
    public function obtenerPrecioFijoEnvioDom() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_fijo_dom');
        $sql->from('pickit_global');
        $pickit_ship_price_fijo_dom = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_fijo_dom'];

        return $pickit_ship_price_fijo_dom;
    }

    // Devuelve el porcentaje personalizado del precio de envío a domicilio de la base de datos.
    public function obtenerPrecioPorcentualEnvioDom() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_porcentual_dom');
        $sql->from('pickit_global');
        $pickit_ship_price_porcentual_dom = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_porcentual_dom'];

        return $pickit_ship_price_porcentual_dom;
    }

    // Devuelve el tipo de Precio de envío a punto de la base de datos.
    public function obtenerTipoPrecioEnvioPunto() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_opt_punto');
        $sql->from('pickit_global');
        $pickit_ship_price_opt_punto = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_opt_punto'];

        return $pickit_ship_price_opt_punto;
    }

    // Devuelve el Precio Fijo de envío a punto de la base de datos.
    public function obtenerPrecioFijoEnvioPunto() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_fijo_punto');
        $sql->from('pickit_global');
        $pickit_ship_price_fijo_punto = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_fijo_punto'];

        return $pickit_ship_price_fijo_punto;
    }

    // Devuelve el porcentaje personalizado del precio de envío a punto de la base de datos.
    public function obtenerPrecioPorcentualEnvioPunto() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_porcentual_punto');
        $sql->from('pickit_global');
        $pickit_ship_price_porcentual_punto = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_porcentual_punto'];

        return $pickit_ship_price_porcentual_punto;
    }

    public function obtenerPrecioPlusEnvioPunto() {
        $sql = new DbQuery();
        $sql->select('pickit_ship_price_plus_punto');
        $sql->from('pickit_global');
        $pickit_ship_price_plus_punto = Db::getInstance()->executeS($sql)[0]['pickit_ship_price_plus_punto'];
        
        return $pickit_ship_price_plus_punto;
    }

    // Devuelve el estado actual de la base de datos.
    public function obtenerEstadoInicial() {
        $sql = new DbQuery();
        $sql->select('pickit_estado_inicial');
        $sql->from('pickit_global');
        $pickit_estado_inicial = Db::getInstance()->executeS($sql)[0]['pickit_estado_inicial'];

        return $pickit_estado_inicial;
    }

    // Devuelve el tipo del peso de producto (kg, g, libra) de la base de datos.
    public function obtenerTipoPesoProducto() {
        $sql = new DbQuery();
        $sql->select('pickit_product_weight');
        $sql->from('pickit_global');
        $pickit_product_weight = Db::getInstance()->executeS($sql)[0]['pickit_product_weight'];

        return $pickit_product_weight;
    }

    // Devuelve el tipo de las dimensiones del producto (cm, m) de la base de datos.
    public function obtenerTipoDimensionProducto() {
        $sql = new DbQuery();
        $sql->select('pickit_product_dim');
        $sql->from('pickit_global');
        $pickit_product_dim = Db::getInstance()->executeS($sql)[0]['pickit_product_dim'];

        return $pickit_product_dim;
    }

    public function obtenerServicioDropOff() {
        $sql = new DbQuery();
        $sql->select('pickit_servicio_drop_off');
        $sql->from('pickit_global');
        $pickit_servicio_drop = Db::getInstance()->executeS($sql)[0]['pickit_servicio_drop_off'];

        return $pickit_servicio_drop;
    }
    
    //Devuelve el Id del tipo de precio fijo.
    public function obtenerIdTipoPrecioFijo() {
        return 1;
    } 

    //Devuelve el Id del tipo de precio porcentual.
    public function obtenerIdTipoPrecioPorcentual() {
        return 2;
    } 

    public function obtenerIdTipoPrecioPlus(){
        return 3;
    }

    public function obtenerIdTipoPrecioGratis(){
        return 4;
    }
    
    //Devuelve el Id del tipo de peso kilogramo.
    public function obtenerIdTipoPesoKilo(){
        return 0;
    }

    //Devuelve el Id del tipo de peso libra.
    public function obtenerIdTipoPesoLibra() {
        return 2;
    }

    //Devuelve el Id del tipo de dimensión centímetro.
    public function obtenerIdTipoDimensionCentimetro() {
        return 1;        
    }
    public function obtenerIdTipoDimensionMilimetro(){
        return 0;
    }

    // Devuelve el ID del tipo de operación de domicilio.
    public function obtenerOperationTypeDomicilio() {
        return 2;        
    }

    // Devuelve el ID del tipo de operación de punto.
    public function obtenerOperationTypePunto() {
        return 1;        
    }

    // Devuelve el nombre de los puntos Pickit
    public function obtenerNombreCarriers() {
        $carriers = array();

        $carriers[] = "Pickit - Punto A";
        $carriers[] = "Pickit - Punto B";
        $carriers[] = "Pickit - Punto C";
        $carriers[] = "Pickit - Punto D";
        $carriers[] = "Pickit - Punto E";
        
        return $carriers;
    }

    // Devuelve el tipo de imposición de la base de datos.
    public function obtenerTipoImposicion() {
        $sql = new DbQuery();
        $sql->select('pickit_imposition_available');
        $sql->from('pickit_global');
        $pickit_imposition_available = Db::getInstance()->executeS($sql)[0]['pickit_imposition_available'];

        return $pickit_imposition_available;
    }

    // Inserta en la tabla de "pickit_order".
    public function insertarTablaOrderImpuesta( $orderId, $transaccionId, $urlEtiqueta, $urlTracking, $estadoInicial ) {
        PickitOrderModel::insertarTablaOrderImpuesta( $orderId, $transaccionId, $urlEtiqueta, $urlTracking, $estadoInicial );
    }
    // 1 => Impuesta
    // 0 => No impuesta
    public function insertarTablaOrderNoImpuesta( $orderId ) {
        PickitOrderModel::insertarTablaOrderNoImpuesta( $orderId );
    }

    // Inserta sólo la etiqueta en la tabla de "pickit_order" (Al transaccionId correspondiente).
    public function insertarEtiqueta( $urlEtiqueta, $transaccionId ) {
        PickitOrderModel::insertarEtiqueta( $urlEtiqueta, $transaccionId );
    }

    // Devuelve la etiqueta de la base de datos, correspondiente al orderId.
    public function consultarEtiqueta( $orderId ) {
        $sql = new DbQuery();
        $sql->select('pickit_etiqueta');
        $sql->from('pickit_order');
        $sql->where("pickit_order_id = $orderId");
        $pickit_etiqueta = Db::getInstance()->executeS($sql)[0]['pickit_etiqueta'];

        return $pickit_etiqueta;
    }

    // Devuelve la URL de Tracking de la base de datos.
    public function consultarTracking( $orderId ) {
        $sql = new DbQuery();
        $sql->select('pickit_seguimiento');
        $sql->from('pickit_order');
        $sql->where("pickit_order_id = $orderId");
        $pickit_seguimiento = Db::getInstance()->executeS($sql)[0]['pickit_seguimiento'];

        return $pickit_seguimiento;
    }

    public function consultarEstadoActual( $orderId ) {
        $sql = new DbQuery();
        $sql->select('pickit_estado_actual');
        $sql->from('pickit_order');
        $sql->where("pickit_order_id = $orderId");
        $pickit_estado_actual = Db::getInstance()->executeS($sql)[0]['pickit_estado_actual'];

        return $pickit_estado_actual;
    }

    public function consultarTransaccionId( $orderId ) {
        $sql = new DbQuery();
        $sql->select('pickit_transaccion_id');
        $sql->from('pickit_order');
        $sql->where("pickit_order_id = $orderId");
        $pickit_transaccion_id = Db::getInstance()->executeS($sql)[0]['pickit_transaccion_id'];

        return $pickit_transaccion_id;
    }

    public function cambiarEstadoActualDb( $orderId ) {
        $table = _DB_PREFIX_ . 'pickit_order';

        $sql = "UPDATE $table SET pickit_estado_actual='Disponible para retiro' WHERE pickit_order_id=$orderId";

        if (!Db::getInstance()->execute($sql))
            die('error!');
    }

    // ---------- Empiezan las funciones de Web Service ---------- //

    // Función para generar request al Web Service de Pickit
    public function callPickitWs( $data, $url ) {
        $postdata = json_encode($data);
        
        $ch = curl_init();
        
		$pickit_url = PickitUtilities::obtenerUrlWS();
        $pickit_apikey = PickitUtilities::obtenerApikey();

        $requestUrl = $pickit_url . $url;
        
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            "apiKey:$pickit_apikey"
        ));

        $outputJson = curl_exec($ch);
        $output = json_decode($outputJson, true);

        curl_close($ch);

        return $output;
    }
    // Función para generar request al Web Service V2.0 de Pickit
    public function callNewPickitWs( $data, $url ) {
        $postdata = json_encode($data);
        
        $ch = curl_init();
        
		$pickit_url = PickitUtilities::obtenerUrlWS();
        $pickit_apikey = PickitUtilities::obtenerApikey();
        $pickit_token_id = PickitUtilities::obtenerTokenId();

        $requestUrl = $pickit_url . $url;
        
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            "apiKey:$pickit_apikey",
            "token:$pickit_token_id"
        ));
        
        $outputJson = curl_exec($ch);
        $output = json_decode($outputJson, true);

        curl_close($ch);

        return $output;
    }

    // Devuelve la etiqueta del Web Service.
    public function obtenerEtiqueta( $transaccionId ) {

        $dataEtiqueta = array(
            'transaccionId' => $transaccionId,
        );

        $urlEtiqueta = "/api/ObtenerEtiqueta";

        $requestEtiqueta = PickitUtilities::callPickitWs($dataEtiqueta, $urlEtiqueta);

        return $requestEtiqueta;
    }

    // Cambia el estado de 1 a 2 de la orden del ID transacción enviado.
    public function cambiarEstadoActualWs( $orderId ) {
        $pickit_token_id = PickitUtilities::obtenerTokenId();
        $transaccionId = PickitUtilities::consultarTransaccionId($orderId);

        $dataCambiarEstado = array(
            "tokenId" => $pickit_token_id,
            "transaccionId" => $transaccionId
        );

        $urlCambiarEstado = "/api/DisponibleParaRetiro";

        $requestCambiarEstado = PickitUtilities::callPickitWs($dataCambiarEstado, $urlCambiarEstado);

        return $requestCambiarEstado;
    }

    public function obtenerCotizacion( $products, $cart, $envio ) {
        $listaProductos = array();

        $tipoPesoProducto = PickitUtilities::obtenerTipoPesoProducto();
        $tipoDimensionProducto = PickitUtilities::obtenerTipoDimensionProducto();
        $centimetro = PickitUtilities::obtenerIdTipoDimensionCentimetro();
        $milimetro = PickitUtilities::obtenerIdTipoDimensionMilimetro();

        foreach ($products as $prods => $prod)
        {
            $pprice = $prod["price"];
            $pname = $prod["name"];
            $pweight = $prod["weight"];
            $pwidth = $prod["width"];
            $pheight = $prod["height"];
            $pdepth = $prod["depth"];
            $psku = $prod["reference"];

            if ( $tipoPesoProducto == PickitUtilities::obtenerIdTipoPesoKilo() )
                $pweight *= 1000;
            elseif ( $tipoPesoProducto == PickitUtilities::obtenerIdTipoPesoLibra() )
                $pweight *= 453.5;

            if ( $tipoDimensionProducto == $milimetro ) {
                $plength /= 10;
                $pheight /= 10;
                $pwidth /= 10;
                $tipoDimensionProducto = $centimetro;
            }

            $newProductToAdd = array(
                "name" => $pname,
                "weight" => array(
                    //"amount" => (float)ceil($pweight),
                    "amount" => $pweight ? (float)(ceil($pweight)) : 1,
                    "unit" => "g"
                ),
                "length" => array(
                    //"amount" => (float)$pdepth,
                    "amount" => $pdepth != 0 ? (float)($pdepth) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "height" => array(
                    //"amount" => (float)$pheight,
                    "amount" => $pheight != 0 ? (float)($pheight) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "width" => array(
                    //"amount" => (float)$pwidth,
                    "amount" => $pwidth != 0 ? (float)($pwidth) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "price" => $pprice ? (float)$pprice : 1,
                "sku" => $psku
            );
            array_push($listaProductos, $newProductToAdd);
        }

        //if ($this->context->customer->isLogged()) {

        $customer = new Customer($cart->id_customer);
        $address = new Address ($cart->id_address_delivery);

        $nombre = $customer->firstname;
        $apellido = $customer->lastname;
        $email = $customer->email;
        $tel = $address->phone;
        $dni = $address->dni;

        $dir = $address->address1;
        $ciudad = $address->city;
        $codpost = $address->postcode;

        $pickit_token_id = PickitUtilities::obtenerTokenId();

        if ($envio["tipo"] == "domicilio")
            $dataCotPkt = array (
                "serviceType" => "PP",
                "workflowTag" => "dispatch",
                "operationType" => PickitUtilities::obtenerOperationTypeDomicilio(),
                "retailer" => array(
                    "tokenId" => $pickit_token_id,
                ),
                "products" => $listaProductos,
                "sla" => array(
                    "id" => 1
                ),
                "customer" => array(
                    "name" => $nombre,
                    "lastName" => $apellido,
                    "pid" => (int) $dni,
                    "email" => $email,
                    "phone" => $tel,
                    "address" => array(
                        "postalCode" => $codpost,
                        "address" => $dir,
                        "city" => $ciudad,
                        "province" => ""
                    )
                ),
            );
        else if ($envio["tipo"] == "punto")
            $dataCotPkt = array (
                "serviceType" => "PP",
                "workflowTag" => "dispatch",
                "operationType" => PickitUtilities::obtenerOperationTypePunto(),
                "retailer" => array(
                    "tokenId" => $pickit_token_id,
                ),
                "products" => $listaProductos,
                "sla" => array(
                    "id" => 1
                ),
                "customer" => array(
                    "name" => $nombre,
                    "lastName" => $apellido,
                    "pid" => (int) $dni,
                    "email" => $email,
                    "phone" => $tel,
                    "address" => array(
                        "postalCode" => $codpost,
                        "address" => $dir,
                        "city" => $ciudad,
                        "province" => ""
                    )
                ),
                "pointId" => $envio["idPunto"]
            );
        
        $urlCotPkt = "/apiV2/budget";
        
        $requestCotPkt = PickitUtilities::callNewPickitWs($dataCotPkt, $urlCotPkt);
        
        return $requestCotPkt;
    }
    
    // Devuelve los puntos Pickit asociados al código postal.
    public function obtenerPuntos( $postCode ) {

        //if(!is_user_logged_in()){
        global $woocommerce;
        if ( $woocommerce->customer->changes["shipping"]["postcode"] ){
            $postCode = $woocommerce->customer->changes["shipping"]["postcode"];
        }

        if (!$postCode)
            return;

        $ch = curl_init();

        $url = "/apiV2/map/point";
        
		$pickit_url = PickitUtilities::obtenerUrlWS();
        $pickit_apikey = PickitUtilities::obtenerApikey();
        $pickit_token_id = PickitUtilities::obtenerTokenId();

        $requestUrl = $pickit_url . $url;
        
        $parameters = array (
            "filter.retailer.tokenId"   => $pickit_token_id,
            "filter.postalCode"         => $postCode,
            "orderBy"                   => "rnd"
        );
    
        $headers = array(
            'Content-Type:application/json',
            "apiKey:$pickit_apikey",
            "token:$pickit_token_id"
        );

        curl_setopt($ch, CURLOPT_URL, $requestUrl . '?' . http_build_query($parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $outputJson = curl_exec($ch);
        $output = json_decode($outputJson, true);

        curl_close($ch);

        return $output;
    }

    // Impone la transacción (A domicilio o a punto).
    public function imponerSimplificadoTransaccion ( $products, $cart, $carrier ) {

        switch ( $carrier->name )
        {
            //case "Pickit - Domicilio":
            //    $carrierName = "domicilio";
            //    break;
            case "Pickit - Punto A":
                $carrierName = "punto";
                $idPunto = $_SESSION["pickit"]["idPuntoUno"];
                break;
            case "Pickit - Punto B":
                $carrierName = "punto";
                $idPunto = $_SESSION["pickit"]["idPuntoDos"];
                break;
            case "Pickit - Punto C":
                $carrierName = "punto";
                $idPunto = $_SESSION["pickit"]["idPuntoTres"];
                break;
            case "Pickit - Punto D":
                $carrierName = "punto";
                $idPunto = $_SESSION["pickit"]["idPuntoCuatro"];
                break;
            case "Pickit - Punto E":
                $carrierName = "punto";
                $idPunto = $_SESSION["pickit"]["idPuntoCinco"];
                break;
            default:
                return;
        }

        unset( $_SESSION["pickit"]["idPuntoUno"]  );
        unset( $_SESSION["pickit"]["idPuntoDos"]  );
        unset( $_SESSION["pickit"]["idPuntoTres"] );
        unset( $_SESSION["pickit"]["idPuntoCuatro"] );
        unset( $_SESSION["pickit"]["idPuntoCinco"] );
        //unset( $_SESSION["pickit"]["precioPunto"] );
        //unset( $_SESSION["pickit"]["precioDomicilio"] );

        $orderId = $cart->id;

        $listaProductos = array();

        $tipoPesoProducto = PickitUtilities::obtenerTipoPesoProducto();
        $tipoDimensionProducto = PickitUtilities::obtenerTipoDimensionProducto();
        $centimetro = PickitUtilities::obtenerIdTipoDimensionCentimetro();
        $milimetro = PickitUtilities::obtenerIdTipoDimensionMilimetro();

        foreach ($products as $prods => $prod)
        {
            $pprice = $prod["price"];
            $pname = $prod["name"];
            $pweight = $prod["weight"];
            $pwidth = $prod["width"];
            $pheight = $prod["height"];
            $pdepth = $prod["depth"];
            $psku = $prod["reference"];

            if ( $tipoPesoProducto == PickitUtilities::obtenerIdTipoPesoKilo() )
                $pweight *= 1000;
            elseif ( $tipoPesoProducto == PickitUtilities::obtenerIdTipoPesoLibra() )
                $pweight *= 453.5;

            if ( $tipoDimensionProducto == $milimetro ) {
                $plength /= 10;
                $pheight /= 10;
                $pwidth /= 10;
                $tipoDimensionProducto = $centimetro;
            }

            $newProductToAdd = array(
                "name" => $pname,
                "weight" => array(
                    //"amount" => (float)ceil($pweight),
                    "amount" => $pweight ? (float)(ceil($pweight)) : 1,
                    "unit" => "g"
                ),
                "length" => array(
                    //"amount" => (float)$pdepth,
                    "amount" => $pdepth ? (float)($pdepth) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "height" => array(
                    //"amount" => (float)$pheight,
                    "amount" => $pheight ? (float)($pheight) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "width" => array(
                    //"amount" => (float)$pwidth,
                    "amount" => $pwidth ? (float)($pwidth) : 1,
                    "unit" => $tipoDimensionProducto == $centimetro ? "cm" : "m"
                ),
                "price" => $pprice ? (float)$pprice : 1,
                "sku" => $psku
            );
            array_push($listaProductos, $newProductToAdd);
        }

        $customer = new Customer( $cart->id_customer );
        $address = new Address ( $cart->id_address_delivery );

        $nombre = $customer->firstname;
        $apellido = $customer->lastname;
        $email = $customer->email;
        $phone = $address->phone;
        $dni = $address->dni;

        $dir = $address->address1;
        $ciudad = $address->city;
        $postcode = $address->postcode;

        $pickit_token_id = PickitUtilities::obtenerTokenId();
        $estado_inicial = PickitUtilities::obtenerEstadoInicial();
        $pickit_apikey = PickitUtilities::obtenerApikey();

        session_start();
        /*
        if ( $carrierName == "domicilio" )
            $dataImp = array(
                "budgetPetition" => array(
                    "serviceType" => "PP",
                    "workflowTag" => 'dispatch',
                    "operationType" => PickitUtilities::obtenerOperationTypeDomicilio(),
                    "retailer" => array(
                        "tokenId" => $pickit_token_id
                    ),
                    "products" => $listaProductos,
                    "sla" => array(
                        "id" => 1
                    ),
                    "customer" => array(
                        "name" => $nombre,
                        "lastName" => $apellido,
                        // DNI
                        "pid" => (int) $dni,
                        "email" => $email,
                        "phone" => $phone,
                        "address" => array(
                            "postalCode" => $postcode,
                            "address" => $dir,
                            "city" => $ciudad,
                            "province" => "provTest"
                        )
                    ),
                ),
                "firstState" => (int)$estado_inicial,
                "trakingInfo" => array(
                "order" => (string)$orderId
                //"shipment" => "string"
                ),
                "packageAmount" => 1
            );
        else */
        if ( $carrierName == "punto" )
            $dataImp = array(
                "budgetPetition" => array(
                    "serviceType" => "PP",
                    "workflowTag" => 'dispatch',
                    "operationType" => PickitUtilities::obtenerOperationTypePunto(),
                    "retailer" => array(
                        "tokenId" => $pickit_token_id
                    ),
                    "products" => $listaProductos,
                    "sla" => array(
                        "id" => 1
                    ),
                    "customer" => array(
                        "name" => $nombre,
                        "lastName" => $apellido,
                        // DNI
                        "pid" => (int) $dni,
                        "email" => $email,
                        "phone" => $phone,
                        "address" => array(
                            "postalCode" => $postcode,
                            "address" => $dir,
                            "city" => $ciudad,
                            "province" => "provTest"
                        )
                    ),
                    "pointId" => $idPunto
                ),
                "firstState" => (int)$estado_inicial,
                "trakingInfo" => array(
                    "order" => (string)$orderId
                ),
                "packageAmount" => 1
            );

        $urlImp = "/apiV2/transaction";

        $requestImp = PickitUtilities::callNewPickitWs($dataImp, $urlImp);

        return $requestImp;
    }
}