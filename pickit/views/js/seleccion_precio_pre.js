$(document).ready(function () {
    var opt = jQuery("#pickit_ship_price").val();
    if (opt == 0 || opt == 4) {
        $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
    } else if (opt == 1) {
        $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().show();
        $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
    } else if (opt == 2) {
        $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().show();
        $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
    } else if (opt == 3) {
        $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
        $('input[name="pickit_ship_price_plus_punto"]').parent().parent().show();
    }

    $("#pickit_ship_price").on('change', function (e) {
        opt = jQuery("#pickit_ship_price").val();
        if (opt == 0 || opt == 4) {
            $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
        } else if (opt == 1) {
            $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().show();
            $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
        } else if (opt == 2) {
            $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().show();
            $('input[name="pickit_ship_price_plus_punto"]').parent().parent().hide();
        } else if (opt == 3) {
            $('input[name="pickit_ship_price_fijo_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_porcentual_punto"]').parent().parent().hide();
            $('input[name="pickit_ship_price_plus_punto"]').parent().parent().show();
        }
    });

});

/**
 * <script type="text/javascript" src="{$module_dir}/views/js/seleccion_precio_envio.js"></script>
 */