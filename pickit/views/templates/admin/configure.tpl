{**
 * 2007-2019 PrestaShop SA and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="panel">
	<h3><i class="icon icon-tags"></i> {l s='Documentación' mod='pickit'}</h3>
	<p>
		&raquo; {l s='Accede a la documentación para configurar este módulo' mod='pickit'} :
		<ul>
			<li><a href="#" target="_blank">{l s='Documentación' mod='pickit'}</a></li>
		</ul>
	</p>
</div>

<script type="text/javascript" src="{$module_dir}/views/js/seleccion_precio_pre.js"></script>
<script type="text/javascript" src="{$module_dir}/views/js/seleccion_pais_tienda.js"></script>