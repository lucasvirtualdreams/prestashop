<?php
/**
 * 2007-2019 PrestaShop SA and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

include_once(_PS_MODULE_DIR_ . 'pickit/classes/PickitGlobalModel.php');

class PickitOrderModel extends ObjectModel
{
	public $pickit_id;
	public $pickit_wc_order_id;
	public $pickit_transaccion_id;
	public $pickit_etiqueta;
	public $pickit_seguimiento;
	public $pickit_estado_actual;
	public static $definition = array(
		'table' => _DB_PREFIX_ . 'pickit_order',
		'primary' => 'pickit_id',
        'multilang' => false,
		'fields' => array(
			'pickit_id'				=>			array('type' => self::TYPE_INT),
			'pickit_order_id'		=>			array('type' => self::TYPE_INT),
			'pickit_transaccion_id'	=> 			array('type' => self::TYPE_INT),
			'pickit_etiqueta'		=> 			array('type' => self::TYPE_STRING),
			'pickit_seguimiento'	=> 			array('type' => self::TYPE_STRING),
			'pickit_estado_actual' => 			array('type' => self::TYPE_INT),
			'pickit_orden_impuesta' => 			array('type' => self::TYPE_INT),
		),
	);

	public function insertarTablaOrderImpuesta( $orderid, $transaccionid, $etiqueta, $seguimiento, $estadoInicial, $ordenImp ) {

		if( $estadoInicial == 1 )
			$estadoInicial = "En retailer";
		elseif ( $estadoInicial == 2)
			$estadoInicial = "Disponible para retiro";
	
		$ambiente = self::getAmbiente();

        // $insertDataImpuesta = array(
        //     'pickit_order_id'       =>  $orderId,
        //     'pickit_transaccion_id' =>  $transaccionId,
        //     'pickit_etiqueta'       =>  $urlEtiqueta,
        //     'pickit_seguimiento'    =>  $urlTracking,
        //     'pickit_estado_actual'  =>  $estadoInicial,
		// 	'pickit_orden_impuesta' =>  "Impuesta",
		// 	'pickit_ambiente'		=>	$ambiente,
        // );
    
        // Db::getInstance()->insert('pickit_order', $insertDataImpuesta);
		
		$sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'pickit_order` (
			pickit_order_id, pickit_transaccion_id, pickit_etiqueta, pickit_seguimiento, pickit_estado_actual, pickit_orden_impuesta, pickit_ambiente
			) VALUES (
				"'.$orderid.'", "'.$transaccionid.'", "'.$etiqueta.'", "'.$seguimiento.'", "'.$estadoInicial.'",  "Impuesta", "'.$ambiente.'")';

		foreach ($sql as $query) {
			if (Db::getInstance()->execute($query) == false) {
				return false;
			}
		}
	}

	public function insertarTablaOrderNoImpuesta( $orderid ) {

		 $ambiente = self::getAmbiente();

		// $insertData = array(
        //     'pickit_order_id'       =>  $orderId,
		// 	'pickit_orden_impuesta' =>  "No Impuesta",
		// 	'pickit_ambiente'		=>	$ambiente,
        // );

        // Db::getInstance()->insert('pickit_order', $insertData);

		$sql[] = 'INSERT INTO `' . _DB_PREFIX_ . 'pickit_order` (
			pickit_order_id, pickit_orden_impuesta, pickit_ambiente
			) VALUES (
				"'.$orderid.'", "No impuesta","'.$ambiente.'" )';

		foreach ($sql as $query) {
			if (Db::getInstance()->execute($query) == false) {
				return false;
			}
		}
	}

	public function insertarEtiqueta( $urlEtiqueta, $transaccionId ) {

		$table = _DB_PREFIX_ . 'pickit_order';
			
		$sql[] = "UPDATE $table SET pickit_etiqueta='$urlEtiqueta' WHERE pickit_transaccion_id=$transaccionId";

		foreach ($sql as $query) {
			if (Db::getInstance()->execute($query) == false) {
				return false;
			}
		}
	}

	public function getAmbiente(){
		/**
		 * ModoTest => 0 - Habilitado
		 * ModoTest => 1 - Deshabilitado
		 */
		return PickitGlobalModel::getModoTestSelect() == 0 ? "Test" : "Produccion"; 
	}
}
