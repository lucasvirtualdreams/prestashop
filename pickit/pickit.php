<?php
/**
 * 2007-2019 PrestaShop SA and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

include_once(_PS_MODULE_DIR_ . 'pickit/classes/PickitGlobalModel.php');
include_once(_PS_MODULE_DIR_ . 'pickit/classes/PickitOrderModel.php');
include_once(_PS_MODULE_DIR_ . 'pickit/classes/PickitUtilities.php');

if (!defined('_PS_VERSION_')) {
    exit;
}
//asdasd
class Pickit extends Module
{

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'pickit';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.1';
        $this->author = 'Pickit';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Pickit');
        $this->description = $this->l('Módulo para envíos a Puntos Pickit.');

        $this->confirmUninstall = $this->l('¿Estás seguro que deseas desinstalar el modulo de Pickit?
        (Esto eliminará las tablas de la base de datos.)');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {

        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $tab = new Tab();
        $tab->active = 0;
        $tab->class_name = 'Ctrl';
        $tab->name = array();

        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = 'Ctrl';

        $tab->id_parent = $id_parent;//if exists
        $tab->position = 0;
        $tab->module = 'pickit';
        $tab->save();

        /*
        $carrier = $this->addCarrier("Pickit - Domicilio", "Envío a domicilio por Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        */
        
        $carrier = $this->addCarrier("Pickit - Punto A", "Envío a un punto Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        $carrier = $this->addCarrier("Pickit - Punto B", "Envío a un punto Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        $carrier = $this->addCarrier("Pickit - Punto C", "Envío a un punto Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        $carrier = $this->addCarrier("Pickit - Punto D", "Envío a un punto Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        $carrier = $this->addCarrier("Pickit - Punto E", "Envío a un punto Pickit");
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);

        Configuration::updateValue('PICKIT_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayCarrierExtraContent') &&
            //$this->registerHook('displayBeforeCarrier') &&
            $this->registerHook('displayBeforeBodyClosingTag') &&
            $this->registerHook('actionValidateOrder') &&
            $this->registerHook('actionAdminOrdersListingFieldsModifier') &&
            $this->registerHook('displayOrderConfirmation') &&
            $this->registerHook('displayOrderDetail') &&
            $this->registerHook('displayAdminOrder');
    }
    
    public function uninstall()
    {
        Configuration::deleteByName('PICKIT_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

	public function getContent()
	{
		$output = null;
	
		if (Tools::isSubmit('submit'.$this->name)) {
			$myModuleName = strval(Tools::getValue('Pickit'));
	
            if (
                !$myModuleName ||
                empty($myModuleName) ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                $this->postProcess();
                Configuration::updateValue('pickit', $myModuleName);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

         /**
         * If values have been submitted in the form, process.
         */
        /*if (((bool)Tools::isSubmit('submitPickitModule')) == true) {
            $this->postProcess();
        }*/

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
	
		$this->postProcess();
		return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    public function renderForm()
    {
        /**Agregado de paises */
        $optionsPais = array(
            array(
                'id_pais_tienda' => 0,
                'name' => 'Argentina'
            ),
            array(
                'id_pais_tienda' => 1,
                'name' => 'Uruguay'
            ),
            array(
                'id_pais_tienda' => 2,
                'name' => 'Mexico'
            ),
            array(
                'id_pais_tienda' => 3,
                'name' => 'Colombia'
            ),
        );

        $optionDimension = array(
            array(
                'pickit_product_dim' => 0,
                'name' => 'Milímetro'
            ),
            array(
                'pickit_product_dim' => 1,
                'name' => 'Centímetros'
            ),
            array(
                'pickit_product_dim' => 2,
                'name' => 'Metros'
            ), 
        );

        $pickit_ship_price_opt_punto = array(
            array(
                'pickit_ship_price_opt_punto' => 0,
                'name' => 'Automático.'
            ),
            array(
                'pickit_ship_price_opt_punto' => 1,
                'name' => 'Fijo.'
            ),    
            array(
                'pickit_ship_price_opt_punto' => 2,
                'name' => 'Porcentaje personalizado.'
            ),
            array(
                'pickit_ship_price_opt_punto' => 3,
                'name' => 'Plus.'
            ),
            array(
                'pickit_ship_price_opt_punto' => 4,
                'name' => 'Gratis.'
            ),                                   
        );

        $fields_form = array(
            'form' => array(
                'name' => 'pickitForm1',
                'legend' => array(
                    'title' => $this->l('Configuración Global'),
                    'icon' => 'icon-cogs',
                    ),
                    'input' => array(

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_testing_mode',
                            'options' => array(
                                'query' => $idtestmode = array(
                                    array(
                                        'idtestmode' => 0,
                                        'name' => 'Habilitado'
                                    ),
                                    array(
                                        'idtestmode' => 1,
                                        'name' => 'Deshabilitado'
                                    ),                                  
                                ),
                                'id' => 'idtestmode',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Modo de Testeo:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_apikey_webapp',
                            'label' => $this->l('ApiKey:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_token_id',
                            'label' => $this->l('Token ID:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_apikey_webapp_test',
                            'label' => $this->l('ApiKey (Test):'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_token_id_test',
                            'label' => $this->l('Token ID (Test):'),
                        ),

                        array(
                            'type' => 'select',
                            'label' => $this->l('Pais de la tienda:'),
                            'name' => 'pickit_pais_tienda',
                            'options' => array(
                                'query' => $optionsPais,
                                'id' => 'id_pais_tienda',
                                'name' => 'name'
                            )
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_url_webservice',
                            'label' => $this->l('URL WebService:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_url_webservice_test',
                            'label' => $this->l('URL WebService (Test):'),
                            'desc' => $this->l(' Se utilizará este WS cuando el Modo Testeo se encuentre habilitado.'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_product_weight',
                            'options' => array(
                                'query' => $pickit_product_weight = array(
                                    array(
                                        'pickit_product_weight' => 0,
                                        'name' => 'Kilogramos'
                                    ),
                                    array(
                                        'pickit_product_weight' => 1,
                                        'name' => 'Gramos'
                                    ),  
                                    array(
                                        'pickit_product_weight' => 2,
                                        'name' => 'Libras'
                                    ),                                         
                                ),
                                'id' => 'pickit_product_weight',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Peso del producto en:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_product_dim',
                            'options' => array(
                                'query' => $optionDimension,
                                'id' => 'pickit_product_dim',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Dimensiones del producto en:'),
                        ),
                        
                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_imposition_available',
                            'options' => array(
                                'query' => $pickit_imposition_available = array(
                                    array(
                                        'pickit_imposition_available' => 0,
                                        'name' => 'Automática según estado definido.'
                                    ),
                                    array(
                                        'pickit_imposition_available' => 1,
                                        'name' => 'Manual al realizar envío.'
                                    ),         
                                ),
                                'id' => 'pickit_imposition_available',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Imposición:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_servicio_drop_off',
                            'label' => $this->l('Servicio Drop Off:'),
                            'options' => array(
                                'query' => $pickit_servicio_drop_off = array(
                                    array(
                                        'pickit_servicio_drop_off' => 0,
                                        'name' => 'Deshabilitado.'
                                    ),
                                    array(
                                        'pickit_servicio_drop_off' => 1,
                                        'name' => 'Habilitado.'
                                    ),                                        
                                ),
                                'id' => 'pickit_servicio_drop_off',
                                'name' => 'name'
                            ),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_estado_inicial',
                            'options' => array(
                                'query' => $pickit_estado_inicial = array(
                                    array(
                                        'pickit_estado_inicial' => 1,
                                        'name' => 'En retailer.'
                                    ),
                                    array(
                                        'pickit_estado_inicial' => 2,
                                        'name' => 'Disponible para retiro.'
                                    ),                                        
                                ),
                                'id' => 'pickit_estado_inicial',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Estado inicial:'),
                            'desc' => $this->l('Este es el estado con el que se hará la imposición.')
                        ),
                        /*
                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_ship_type',
                            'options' => array(
                                'query' => $pickit_ship_type = array(
                                    array(
                                        'pickit_ship_type' => 0,
                                        'name' => 'Envío a punto.'
                                    ),
                                    array(
                                        'pickit_ship_type' => 1,
                                        'name' => 'Envío a domicilio.'
                                    ), 
                                    array(
                                        'pickit_ship_type' => 2,
                                        'name' => 'Ambos.'
                                    ),                                  
                                ),
                                'id' => 'pickit_ship_type',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Tipo de envío:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_ship_price',
                            'options' => array(
                                'query' => $pickit_ship_price_opt_dom = array(
                                    array(
                                        'pickit_ship_price_opt_dom' => 0,
                                        'name' => 'Automático.'
                                    ),
                                    array(
                                        'pickit_ship_price_opt_dom' => 1,
                                        'name' => 'Fijo.'
                                    ),    
                                    array(
                                        'pickit_ship_price_opt_dom' => 2,
                                        'name' => 'Porcentaje personalizado.'
                                    ),                                  
                                ),
                                'id' => 'pickit_ship_price_opt_dom',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Precio de envío a domicilio:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_ship_price_fijo_dom',
                            'label' => $this->l('Precio fijo (Domicilio):'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_ship_price_porcentual_dom',
                            'label' => $this->l('Porcentaje (Domicilio):'),
                        ),
                        */
                        array(
                            'col' => 4,
                            'type' => 'select',
                            'name' => 'pickit_ship_price',
                            'options' => array(
                                'query' => $pickit_ship_price_opt_punto,
                                'id' => 'pickit_ship_price_opt_punto',
                                'name' => 'name'
                            ),
                            'label' => $this->l('Precio de envío a punto:'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_ship_price_fijo_punto',
                            'label' => $this->l('Precio fijo (Punto):'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_ship_price_porcentual_punto',
                            'label' => $this->l('Porcentaje (Punto):'),
                        ),

                        array(
                            'col' => 4,
                            'type' => 'text',
                            'name' => 'pickit_ship_price_plus_punto',
                            'label' => $this->l('Plus (Punto):'),
                        ),
            ),
            'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right',
            'name' => 'pickit_submit_global',
            )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->module = $this;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }
    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('pickit_global');
        $globalvalues = Db::getInstance()->executeS($sql);
        foreach ($globalvalues[0] as $values)
            $globalarray[] = $values;

        // var_dump($globalarray); exit;
        if(!$globalarray[7]){
            $webService = "https://api.pickit.net";
            $webServiceTest = "https://api.pickitlabs.com";
        }else {
            $webService = $globalarray[7];
            $webServiceTest = $globalarray[8];
        }

        return array(
            'pickit_testing_mode' => $globalarray[1],
            'pickit_apikey_webapp' => $globalarray[2],
            'pickit_token_id' => $globalarray[3],
            'pickit_apikey_webapp_test' => $globalarray[4],
            'pickit_token_id_test' => $globalarray[5],
            'pickit_pais_tienda' => $globalarray[6],
            'pickit_url_webservice' => $webService,
            'pickit_url_webservice_test' => $webServiceTest,
            'pickit_product_weight' => $globalarray[9],
            'pickit_product_dim' => $globalarray[10],
            'pickit_imposition_available' => $globalarray[11],
            'pickit_estado_inicial' => $globalarray[12],
            //'pickit_ship_type' => $globalarray[11],
            //'pickit_ship_price_opt_dom' => $globalarray[12],
            //'pickit_ship_price_fijo_dom' => $globalarray[13],
            //'pickit_ship_price_porcentual_dom' => $globalarray[14],
            'pickit_ship_price' => $globalarray[13],
            'pickit_ship_price_fijo_punto' => $globalarray[14],
            'pickit_ship_price_porcentual_punto' => $globalarray[15],
            'pickit_ship_price_plus_punto'  => $globalarray[16],
            'pickit_servicio_drop_off' => $globalarray[17]
        );
    }

    /**
     * Save form data.
     */
    private $patron_texto = "0-9";

    protected function postProcess()
    {                
        if (Tools::isSubmit('pickit_submit_global')) {

            $testmode = Tools::getValue('pickit_testing_mode');
            $apikw = Tools::getValue('pickit_apikey_webapp');
            $tokenid = Tools::getValue('pickit_token_id');
            $urlws = Tools::getValue('pickit_url_webservice');
            $urlwst = Tools::getValue('pickit_url_webservice_test');
            $weight = Tools::getValue('pickit_product_weight');
            $dim = Tools::getValue('pickit_product_dim');
            $impost = Tools::getValue('pickit_imposition_available');
            $estadoInicial = Tools::getValue('pickit_estado_inicial');
            //$shiptype = Tools::getValue('pickit_ship_type');
            //$shipdomopt = Tools::getValue('pickit_ship_price_opt_dom');
            //$shipdomfijo = Tools::getValue('pickit_ship_price_fijo_dom');
            //$shipdomporc = Tools::getValue('pickit_ship_price_porcentual_dom');
            $shippuntoopt = Tools::getValue('pickit_ship_price');
            $shippuntofijo = Tools::getValue('pickit_ship_price_fijo_punto');
            $shippuntoporc = Tools::getValue('pickit_ship_price_porcentual_punto');

            //Agregado
            $paisTienda = Tools::getValue('pickit_pais_tienda');
            $apikwTest = Tools::getValue('pickit_apikey_webapp_test');
            $tokenidTest = Tools::getValue('pickit_token_id_test');
            $plus = Tools::getValue('pickit_ship_price_plus_punto');
            $servicioDropOff = Tools::getValue('pickit_servicio_drop_off');

            //Validacion de Input
            if( ($shippuntoopt == PickitUtilities::obtenerIdTipoPrecioFijo() && !is_numeric($shippuntofijo)) ||
                ($shippuntoopt == PickitUtilities::obtenerIdTipoPrecioPorcentual() && !is_numeric($shippuntoporc)) ||
                ($shippuntoopt == PickitUtilities::obtenerIdTipoPrecioPlus() && !is_numeric($plus))
            ){
                $this->adminDisplayWarning('El "Precio de envío a punto" ingresado debe ser númerico.');
                return;
            }

            $configGlobal = new PickitGlobalModel();
            
			$configGlobal->saveGlobalData($testmode, $apikw, $tokenid, $urlws, $urlwst, $weight, $dim,
            $impost, $estadoInicial, /*$shiptype, $shipdomopt, $shipdomfijo, $shipdomporc,*/ $shippuntoopt, $shippuntofijo, $shippuntoporc,
            $paisTienda, $apikwTest, $tokenidTest, $plus, $servicioDropOff);
            			
			$configGlobal->save();

			$sql = array();
			
			foreach ($sql as $query) {
				if (Db::getInstance()->execute($query) == false) {
					return false;
				}
			}
        }
    }

    public function getPackageShippingCost($cart, $shipping_cost, $products, $carrier) {
        global $smarty;
     
        if(!isset($_SESSION)) {
            session_start();
        }

        if ($this->context->controller->php_self == "cart"  ||
            $this->context->controller->php_self == "order" ) {
               
            /*
            if ( $carrier->name == "Pickit - Domicilio" ) {

                if ($_SESSION["carrierDomicilioHizoRequest"]) {

                    $_SESSION["carrierDomicilioHizoRequest"] = false;
    
                    $idPrecioFijo = PickitUtilities::obtenerIdTipoPrecioFijo(); 	
                    $idPrecioPorcentual = PickitUtilities::obtenerIdTipoPrecioPorcentual();
                            
                    $_SESSION["empezarCotizacion"] = true;
                            
                    $envio = array(
                        "tipo" => "domicilio"
                    );
    
                    $requestDomPkt = PickitUtilities::obtenerCotizacion($products, $cart, $envio);
    
                    $rateDomPkt = $requestDomPkt["price"];
    
                    $tipoPrecioEnvio = PickitUtilities::obtenerTipoPrecioEnvioDom();
    
                    if	($tipoPrecioEnvio == $idPrecioFijo ){
                        $rateDomPkt = PickitUtilities::obtenerPrecioFijoEnvioDom();
                    }
                    elseif	($tipoPrecioEnvio == $idPrecioPorcentual ){
                        $rateDomPkt *= PickitUtilities::obtenerPrecioPorcentualEnvioDom();
                        $rateDomPkt = (float)number_format($rateDomPkt, 2);
                    }
    
                    $_SESSION["pickit"]["precioDomicilio"] = $rateDomPkt;
                    return $_SESSION["pickit"]["precioDomicilio"];
                }
                return $_SESSION["pickit"]["precioDomicilio"];
            } else*/

            /*
            if (    $carrier->name == "Pickit - Punto A " ||
            $carrier->name == "Pickit - Punto B" ||
            $carrier->name == "Pickit - Punto C" ||
            $carrier->name == "Pickit - Punto D" ||
            $carrier->name == "Pickit - Punto E" ){
                */
                
            if ( in_array ( $carrier->name, PickitUtilities::obtenerNombreCarriers() ) ) {
                if ( $_SESSION["carrierPuntoHizoRequest"] ) {

                    $_SESSION["carrierPuntoHizoRequest"] = false;
                    //$_SESSION["carrierPuntoHizoRequest2"] = true;

                    $idPrecioFijo = PickitUtilities::obtenerIdTipoPrecioFijo(); 	
                    $idPrecioPorcentual = PickitUtilities::obtenerIdTipoPrecioPorcentual();
                    $idPrecioPlus =  PickitUtilities::obtenerIdTipoPrecioPlus();
                    $idPrecioGratis = PickitUtilities::obtenerIdTipoPrecioGratis();

                    $_SESSION["empezarCotizacion"] = true;

                    $address = new Address ($cart->id_address_delivery);
                    $postCode = $address->postcode;

                    $puntosRequest = PickitUtilities::obtenerPuntos($postCode);
                    
                    $puntos["result"] = array();

                    foreach ($puntosRequest["result"] as $punto)
                        if ($punto["serviceType"] == "PP")
                            array_push($puntos["result"], $punto);
                                        
                    unset( $_SESSION["pickit"]["hayPuntoUno"] );
                    unset( $_SESSION["pickit"]["hayPuntoDos"] );
                    unset( $_SESSION["pickit"]["hayPuntoTres"] );
                    unset( $_SESSION["pickit"]["hayPuntoCuatro"] );
                    unset( $_SESSION["pickit"]["hayPuntoCinco"] );

                    // Si hay al menos un punto disponible.
                    if ( $puntos["result"] ) {

                        $punto = $puntos["result"][0];
                        $idPunto = $punto["idService"];
                        
                        $_SESSION["pickit"]["idPuntoUno"] = $idPunto;
                        $_SESSION["pickit"]["nombrePuntoUno"] = $punto["name"] . ", " . $punto["address"] . ", CP: " . $punto["postalCode"];
                        $smarty->clearAssign('nombrePuntoUno');
                        $smarty->assign('nombrePuntoUno', $_SESSION["pickit"]["nombrePuntoUno"]);
                        $_SESSION["pickit"]["hayPuntoUno"] = true;

                        $envio = array(
                            "tipo" => "punto",
                            "idPunto" => $idPunto
                        );

                        $requestPuntoPkt = PickitUtilities::obtenerCotizacion( $products, $cart, $envio );

                        $ratePuntoPkt = $requestPuntoPkt["price"];

                        $tipoPrecioEnvio = PickitUtilities::obtenerTipoPrecioEnvioPunto();

                        switch( $tipoPrecioEnvio ){
                            case $idPrecioFijo:
                                $ratePuntoPkt = PickitUtilities::obtenerPrecioFijoEnvioPunto();
                                break;
                            case $idPrecioPorcentual:
                                $ratePuntoPkt *= PickitUtilities::obtenerPrecioPorcentualEnvioPunto()/100;
                                $ratePuntoPkt = (float)number_format($ratePuntoPkt, 2);
                                break;
                            case $idPrecioPlus:
                                $ratePuntoPkt += PickitUtilities::obtenerPrecioPlusEnvioPunto();
                                break;
                            case $idPrecioGratis:
                                $ratePuntoPkt = 0;
                                break;
                        }

                        //Prueba
                        //$ratePuntoPkt = rand(1,100);

                        // if		( $tipoPrecioEnvio == $idPrecioFijo ){
                        //     $ratePuntoPkt = PickitUtilities::obtenerPrecioFijoEnvioPunto();
                        // }
                        // elseif	( $tipoPrecioEnvio == $idPrecioPorcentual ){
                        //     $ratePuntoPkt *= PickitUtilities::obtenerPrecioPorcentualEnvioPunto()/100;
                        //     $ratePuntoPkt = (float)number_format($ratePuntoPkt, 2);
                        // }

                        
                        if ( $puntos["result"][1] ) {
                            $_SESSION["pickit"]["hayPuntoDos"] = true;
                            $_SESSION["pickit"]["idPuntoDos"] = $puntos["result"][1]["idService"];
                            $_SESSION["pickit"]["nombrePuntoDos"] = $puntos["result"][1]["name"] . ", " . $puntos["result"][1]["address"] . ", CP: " . $puntos["result"][1]["postalCode"];
                            $smarty->clearAssign('nombrePuntoDos');
                            $smarty->assign('nombrePuntoDos', $_SESSION["pickit"]["nombrePuntoDos"] );

                            if ( $puntos["result"][2] ) {
                                $_SESSION["pickit"]["hayPuntoTres"] = true;
                                $_SESSION["pickit"]["idPuntoTres"] = $puntos["result"][2]["idService"];
                                $_SESSION["pickit"]["nombrePuntoTres"] = $puntos["result"][2]["name"] . ", " . $puntos["result"][2]["address"] . ", CP: " . $puntos["result"][2]["postalCode"];
                                $smarty->clearAssign('nombrePuntoTres');
                                $smarty->assign('nombrePuntoTres', $_SESSION["pickit"]["nombrePuntoTres"] );

                                if ( $puntos["result"][3] ) {
                                    $_SESSION["pickit"]["hayPuntoCuatro"] = true;
                                    $_SESSION["pickit"]["idPuntoCuatro"] = $puntos["result"][3]["idService"];
                                    $_SESSION["pickit"]["nombrePuntoCuatro"] = $puntos["result"][3]["name"] . ", " . $puntos["result"][3]["address"] . ", CP: " . $puntos["result"][3]["postalCode"];
                                    $smarty->clearAssign('nombrePuntoCuatro');
                                    $smarty->assign('nombrePuntoCuatro', $_SESSION["pickit"]["nombrePuntoCuatro"]);

                                    if ( $puntos["result"][4] ) {
                                        $_SESSION["pickit"]["hayPuntoCinco"] = true;
                                        $_SESSION["pickit"]["idPuntoCinco"] = $puntos["result"][4]["idService"];
                                        $_SESSION["pickit"]["nombrePuntoCinco"] = $puntos["result"][4]["name"] . ", " . $puntos["result"][4]["address"] . ", CP: " . $puntos["result"][4]["postalCode"];
                                        $smarty->clearAssign('nombrePuntoCinco');
                                        $smarty->assign('nombrePuntoCinco', $_SESSION["pickit"]["nombrePuntoCinco"]);
                                    }
                                }
                            }
                        }
                        $_SESSION["pickit"]["precioPunto"] = $ratePuntoPkt;
                    } else  {
                        unset($_SESSION["hayPuntoUno"]);
                    }
                } else/* if ( $_SESSION["carrierPuntoHizoRequest2"] ) */ {

                    if ( !$_SESSION["pickit"]["hayPuntoUno"] )
                        return false;
                    
                    if ( !$_SESSION["pickit"]["hayPuntoDos"]     && ($carrier->name == "Pickit - Punto B" ||
                                                                        $carrier->name == "Pickit - Punto C" ||
                                                                        $carrier->name == "Pickit - Punto D" ||
                                                                        $carrier->name == "Pickit - Punto E" ))
                        return false;
    
                    if ( !$_SESSION["pickit"]["hayPuntoTres"]    && ($carrier->name == "Pickit - Punto C" ||
                                                                        $carrier->name == "Pickit - Punto D" ||
                                                                        $carrier->name == "Pickit - Punto E" ))
                        return false;
    
                    if ( !$_SESSION["pickit"]["hayPuntoCuatro"]  && ($carrier->name == "Pickit - Punto D" ||
                                                                        $carrier->name == "Pickit - Punto E" ))
                        return false;
                        
                    if ( !$_SESSION["pickit"]["hayPuntoCinco"]   &&   $carrier->name == "Pickit - Punto E" )
                        return false;

                    if ( $_SESSION["pickit"]["hayPuntoUno"] ) {
                        $smarty->clearAssign( 'nombrePuntoUno' );
                        $smarty->assign( 'nombrePuntoUno', $_SESSION["pickit"]["nombrePuntoUno"] );

                        if ( $_SESSION["pickit"]["hayPuntoDos"] ) {
                            $smarty->clearAssign( 'nombrePuntoDos' );
                            $smarty->assign( 'nombrePuntoDos', $_SESSION["pickit"]["nombrePuntoDos"] );

                            if ( $_SESSION["pickit"]["hayPuntoTres"] ) {
                                $smarty->clearAssign( 'nombrePuntoTres' );
                                $smarty->assign( 'nombrePuntoTres', $_SESSION["pickit"]["nombrePuntoTres"] );

                                if ( $_SESSION["pickit"]["hayPuntoCuatro"] ) {
                                    $smarty->clearAssign( 'nombrePuntoCuatro' );
                                    $smarty->assign( 'nombrePuntoCuatro', $_SESSION["pickit"]["nombrePuntoCuatro"] );

                                    if ( $_SESSION["pickit"]["hayPuntoCinco"] ) {
                                        $smarty->clearAssign('nombrePuntoCinco');
                                        $smarty->assign( 'nombrePuntoCinco', $_SESSION["pickit"]["nombrePuntoCinco"] );
                                    }
                                }
                            }
                        }
                    }
               } //FIN ELSE IF CARRIER PUNTO REQUEST

            }
            return $_SESSION["pickit"]["precioPunto"];
        } elseif (  $this->context->controller->php_self == "order-confirmation" ) {
            $precioPunto = $_SESSION["pickit"]["precioPunto"];
            unset( $_SESSION["pickit"]["precioPunto"] );
            return $precioPunto;
        }
        
        return $_SESSION["pickit"]["precioPunto"];
        
        /*
        if ( $carrier->name == "Pickit - Domicilio" )
            return $_SESSION["pickit"]["precioDomicilio"];
        else*/
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        /*
        if (Context::getContext()->customer->logged == true){
            session_start();
            if ($_SESSION["carrierDomicilioHizoRequest"]==true){

                if ($this->context->controller->php_self=="carta" ||
                    $this->context->controller->php_self=="ordera" ) {

                    $id_address_delivery = Context::getContext()->cart->id_address_delivery;
                    $address = new Address($id_address_delivery);

                    $listaProductos = array();

                    $newProductToAdd = array(
                        "sku" => $psku,
                        "articulo" => $pname,
                        "valorDeclarado" => $pprice,
                        "alto" => $pheight,
                        "ancho" => $pwidth,
                        "largo" => $plength,
                        "peso" => $pweight
                    );
                            
                    //$pais = $package["destination"]["country"];
                    //$provinciaId = $package["destination"]["state"];
                    $codpost = $package["destination"]["postcode"];
                    $ciudad = $package["destination"]["city"];
                    $dir = $package["destination"]["address"];
                    $dir2 = $package["destination"]["address_1"];
                    $dir3 = $package["destination"]["address_2"];

                    $dataDomPkt = array(
                        'tokenId' => 'YRG7VW2Q5V',
                        'articulos' => array (
                            array (
                                "sku" => "112233",
                                "articulo" => "Remera Nike",
                                "valorDeclarado" => 550,
                                "alto" => 14,
                                "ancho" => 40,
                                "largo" => 34,
                                "peso" => 15
                            ),
                        ),
                        "dataDireccionAlternativa" => array(
                            "provinciaId" => "",
                            "direccion" => "",
                            "localidad" => "",
                            "codigoPostal" => "",
                        ),
                        "dataCliente" => array(
                            "nombre" => "Ricardo",
                            "apellido" => "Ebhardt",
                            "dni" => "30745845",
                            "direccion" => "direc",
                            "localidad" => "ciudad",
                            "codigoPostal" => "codpost",
                            "email" => "rebhardt@pickit.net",
                            "telefono" => "11318078946"
                        ),
                    );

                    //Obtiene cotizationId y ValorTransacción de Pickit a Domicilio
                    $postdataDomPkt = json_encode($dataDomPkt);

                    $urlDomPkt = "https://api.pickitlabs.com/api/ObtenerCotizacionEnvioDomicilio";
                    $chDomPkt = curl_init();
                            
                    curl_setopt($chDomPkt, CURLOPT_URL, $urlDomPkt);
                    curl_setopt($chDomPkt, CURLOPT_HEADER, 0);
                    curl_setopt($chDomPkt, CURLOPT_RETURNTRANSFER, true); 
                    curl_setopt($chDomPkt, CURLOPT_POST, true);
                    curl_setopt($chDomPkt, CURLOPT_POSTFIELDS, $postdataDomPkt);
                    curl_setopt($chDomPkt, CURLOPT_HTTPHEADER, array(
                                'Content-Type:application/json',
                                'apiKey:ZN1RW43VO4'
                    ));

                    $outputJsonDomPkt = curl_exec($chDomPkt);
                    $outputDomPkt = json_decode($outputJsonDomPkt, true);

                    $precioDomPkt = $outputDomPkt;

                    curl_close($chDomPkt);

                    $_SESSION["cotizacionId"] = $precioDomPkt["Response"]["cotizacionId"];
                    $rateDomPkt = $precioDomPkt["Response"]["ValorTransaccion"];

                    $_SESSION["carrierDomValue"] = $rateDomPkt;
                    $_SESSION["carrierDomicilioHizoRequest"] = false;
                }

            $carrier = new Carrier(Context::getContext()->cart->id_carrier);
            $nombrecarrier = $carrier->name;
            if ($nombrecarrier == "Pickit - Punto")
                $_SESSION["carrierDomValue"] = 50;
            else
                $_SESSION["carrierDomValue"] = 100;
            }

            return rand(1, 10);
            //return $_SESSION["carrierDomValue"];
            //return $rateDomPkt;
        }
        return $shipping_cost; */
    }

    public function getOrderShippingCostExternal($params)
    {
        return true;
    }

    protected function addCarrier($name, $description)
    {
        $carrier = new Carrier();
        $carrier->name = $name;
        $carrier->is_module = true;
        $carrier->active = 1;
        $carrier->range_behavior = 1;
        $carrier->need_range = 1;
        $carrier->shipping_external = true;
        $carrier->range_behavior = 0;
        $carrier->external_module_name = $this->name;
        $carrier->shipping_method = 2;

        foreach (Language::getLanguages() as $lang)
            //$carrier->delay[$lang['id_lang']] = $this->l('Super fast delivery');
            $carrier->delay[$lang['id_lang']] = $description;

        if ($carrier->add() == true)
        {
            @copy(dirname(__FILE__).'/views/img/carrier_image.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
            Configuration::updateValue('PICKIT_CARRIER_ID', (int)$carrier->id);
            return $carrier;
        }

        return false;
    }

    protected function addGroups($carrier)
    {
        $groups_ids = array();
        $groups = Group::getGroups(Context::getContext()->language->id);
        foreach ($groups as $group)
            $groups_ids[] = $group['id_group'];

        $carrier->setGroups($groups_ids);
    }

    protected function addRanges($carrier)
    {
        $range_price = new RangePrice();
        $range_price->id_carrier = $carrier->id;
        $range_price->delimiter1 = '0';
        $range_price->delimiter2 = '10000';
        $range_price->add();

        $range_weight = new RangeWeight();
        $range_weight->id_carrier = $carrier->id;
        $range_weight->delimiter1 = '0';
        $range_weight->delimiter2 = '10000';
        $range_weight->add();
    }

    protected function addZones($carrier)
    {
        $zones = Zone::getZones();

        foreach ($zones as $zone)
            $carrier->addZone($zone['id_zone']);
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/back.js');
        $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    //Contenido de la vista de los Carriers
    public function hookDisplayCarrierExtraContent( $carriers )
    {
        
        switch ( $carriers["carrier"]["name"] )
        {
            case 'Pickit - Punto A':
                return $this->display(__FILE__, 'views/templates/hook/vistapuntopickitA_S.tpl');
                break;

            case 'Pickit - Punto B':
                return $this->display(__FILE__, 'views/templates/hook/vistapuntopickitB.tpl');
                break;

            case 'Pickit - Punto C':
                return $this->display(__FILE__, 'views/templates/hook/vistapuntopickitC.tpl');
                break;

            case 'Pickit - Punto D':
                return $this->display(__FILE__, 'views/templates/hook/vistapuntopickitD.tpl');
                break;

            case 'Pickit - Punto E':
                return $this->display(__FILE__, 'views/templates/hook/vistapuntopickitE.tpl');
                break;
        }

        /*
        if( $carriers["carrier"]["name"] == 'Pickit - Punto A' ||
            $carriers["carrier"]["name"] == 'Pickit - Punto B' ||
            $carriers["carrier"]["name"] == 'Pickit - Punto C' ||
            $carriers["carrier"]["name"] == 'Pickit - Punto D' ||
            $carriers["carrier"]["name"] == 'Pickit - Punto E' )
        
            //Pickit - Punto
        
        elseif ( $carriers["carrier"]["name"] == "Pickit - Domicilio" ){
            //A Domicilio
            return $this->display(__FILE__, 'views/templates/hook/vistadomiciliopickit.tpl');
        }
        */
    }

    //Modal
    public function hookDisplayBeforeBodyClosingTag()
    {
        //$_SESSION["carrierDomicilioHizoRequest"] = true;
        $_SESSION["carrierPuntoHizoRequest"] = true;
        //return $this->display(__FILE__, 'views/templates/hook/modalpickit.tpl');
    }

    /*
    public function hookDisplayBeforeCarrier( $param )
    {
        return $this->display(__FILE__, 'views/templates/hook/vistapuntopickit.tpl');
    }
    */

    public function hookActionValidateOrder ( $params )
    {
        unset( $_SESSION["empezarCotizacion"] );

        $carrier = new Carrier( $params["order"]->id_carrier );

        if ( in_array ( $carrier->name, PickitUtilities::obtenerNombreCarriers() ) ) {
            
            $orderId = $params["order"]->id_cart;

            if ( PickitUtilities::obtenerTipoImposicion() ) {

                PickitUtilities::insertarTablaOrderNoImpuesta ( $orderId );

                unset( $_SESSION["pickit"]["precioPunto"] );
                //unset( $_SESSION["pickit"]["precioDomicilio"] );
                return;
            }

            $cart = $params["cart"];
            $products = $cart->getProducts();

            $requestImp = PickitUtilities::imponerSimplificadoTransaccion( $products, $cart, $carrier );
                
            $estadoInicial = PickitUtilities::obtenerEstadoInicial();
            $transaccionId = $requestImp["transactionId"];
            $urlTracking = $requestImp["urlTracking"];

            $numberTracking = explode( "trackingCode=", $urlTracking );
            $params["order"]->setWsShippingNumber( $numberTracking[1] );

            
            $responseCode = "";
            $count = 0;
                
            // Si en 5 oportunidades la etiqueta no pudo ser recuperada, se ignora y se pasa al siguiente paso.
            // (La etiqueta se recuperará más tarde.)
            while ( $responseCode != "200" && $count < 5 ) {
                $count++;
                // Espera dos segundos para que el Web Service genere la etiqueta.
                sleep(2);
                $etiqueta = PickitUtilities::obtenerEtiqueta($transaccionId);
                $responseCode = $etiqueta["Status"]["Code"];
            }

            if ($responseCode == "200")
                $urlEtiqueta = $etiqueta["Response"]["UrlEtiqueta"][0];

            PickitUtilities::insertarTablaOrderImpuesta ( $orderId, $transaccionId, $urlEtiqueta, $urlTracking, $estadoInicial );
        }

    }

    public function hookActionAdminOrdersListingFieldsModifier ( $params ) {
        if ( isset( $params['select'] ) ) {
            
            /**
             * Modo Test => 0 - Habilitado
             * Modo Test => 1 - Deshabilitado
             * Al tener un NOT IN en la consulta manejo el filtro por la logica contraria, si es TEST evito mostrar PRODUCCION y viceversa.
             */

            $modoTestSeleccionado = PickitUtilities::getModoTestSelec();
            $filtroAmbiente = $modoTestSeleccionado == 0 ? "Produccion" : "Test"; 

            $params['join'] .= 'LEFT JOIN '._DB_PREFIX_.'pickit_order as porder ON (a.id_order = porder.pickit_order_id)';
            $params['select'] .= ', porder.pickit_estado_actual, porder.pickit_orden_impuesta';
            $params['where'] .= ' AND a.id_order NOT IN (SELECT Ord.pickit_order_id FROM '._DB_PREFIX_.'pickit_order AS Ord WHERE Ord.pickit_ambiente = "'.$filtroAmbiente.'") ';
            
            /*
            $params['select'] .= ', porder.pickit_etiqueta, porder.pickit_estado_actual, porder.pickit_orden_impuesta';
            
            $params['fields']['pickit_etiqueta'] = array(
                'title' => 'Pickit - Etiqueta',
                'align' => 'center',
                'search' => true,
                'havingFilter' => true,
                'filter_key' => 'porder!pickit_etiqueta',
                'prefix' => '<a class="btn btn-primary link-etiqueta-pickit" rel="noopener noreferrer" target="_blank" href="',
                'suffix' => '">Etiqueta</a>'
            );
            */

            $params['fields']['pickit_estado_actual'] = array(
                'title' => 'Pickit - Estado Actual',
                'align' => 'center',
                'search' => true,
                'havingFilter' => true,
                'filter_key' => 'porder!pickit_estado_actual',
                //'callback' => 'pickitEstadoActual',
                //'callback_object' => Module::getInstanceByName('Pickit'),
            );

            $params['fields']['pickit_orden_impuesta'] = array(
                'title' => 'Pickit - Orden impuesta',
                'align' => 'center',
                'search' => true,
                'havingFilter' => true,
                'filter_key' => 'porder!pickit_orden_impuesta'
            );
        }
    }

    public function hookDisplayOrderConfirmation ( $params ) { 
        global $smarty;

        $urlTrackingPickit = PickitUtilities::consultarTracking((int)$params["order"]->id_cart);

        if ( !$urlTrackingPickit )
            return;

        $nombreCarrier = (new Carrier($params["order"]->id_carrier))->name;

        session_start();

        switch ($nombreCarrier) {
            case "Pickit - Punto A":
                $nombrePuntoPickit = $_SESSION["pickit"]["nombrePuntoUno"];
                break;
            case "Pickit - Punto B":
                $nombrePuntoPickit = $_SESSION["pickit"]["nombrePuntoDos"];
                break;
            case "Pickit - Punto C":
                $nombrePuntoPickit = $_SESSION["pickit"]["nombrePuntoTres"];
                break;
            case "Pickit - Punto D":
                $nombrePuntoPickit = $_SESSION["pickit"]["nombrePuntoCuatro"];
                break;
            case "Pickit - Punto E":
                $nombrePuntoPickit = $_SESSION["pickit"]["nombrePuntoCinco"];
        }
        
        $smarty->clearAssign( 'urlTrackingPickit' );
        $smarty->assign( 'urlTrackingPickit', $urlTrackingPickit );

        $smarty->clearAssign( 'nombreCarrier' );
        $smarty->assign( 'nombreCarrier', $nombreCarrier );

        $smarty->clearAssign( 'nombrePuntoPickit' );
        $smarty->assign( 'nombrePuntoPickit', $nombrePuntoPickit );

        return $this->display(__FILE__, 'views/templates/hook/vistaorderconfirmation.tpl');
    }

    public function hookDisplayOrderDetail ( $params ) {
        global $smarty;

        $orderId = (int)$params["order"]->id_cart;

        $urlTrackingPickit = PickitUtilities::consultarTracking( $orderId );

        if ( !$urlTrackingPickit )
            return;

        $smarty->clearAssign( 'urlTrackingPickit' );
        $smarty->assign( 'urlTrackingPickit', $urlTrackingPickit );

        return $this->display(__FILE__, 'views/templates/hook/vistaorderdetail.tpl');
    }

    public function hookDisplayAdminOrder( $params )
    {
        global $smarty;

        $orderId = (int)$params["id_order"];

        $estadoActualOrder = PickitUtilities::consultarEstadoActual( $orderId );
        
        $urlEtiqueta = PickitUtilities::consultarEtiqueta( $orderId );
        $transaccionId = (int)PickitUtilities::consultarTransaccionId( $orderId );

        if ( !$transaccionId )
            return;

        if ( !$urlEtiqueta ) {

            $etiqueta = PickitUtilities::obtenerEtiqueta( $transaccionId );
            $responseCode = $etiqueta["Status"]["Code"];

            if ( $responseCode == "200" ) {
                $urlEtiqueta = $etiqueta["Response"]["UrlEtiqueta"][0];
                PickitUtilities::insertarEtiqueta( $urlEtiqueta, $transaccionId );
            }
        }

        $smarty->clearAssign( 'urlEtiqueta' );
        $smarty->assign( 'urlEtiqueta', $urlEtiqueta );

        if ( $estadoActualOrder == "En retailer" ) {

            if (isset($_POST['pickit_cambiar_estado'])){

                /**
                 * ServicioDropOff => 0 - Deshabilitado
                 * ServicioDropOff => 1 - Habilitado
                 */
                if( PickitUtilities::obtenerServicioDropOff() ) {
                    $this->adminDisplayWarning('No es posible cambiar el estado de la orden a "Disponible para retiro". El Servicio Drop Off se encuentra habilitado.');
                    return $this->display(__FILE__, 'views/templates/hook/vistaadminorderdetail.tpl');
                }

                $requestWs = PickitUtilities::cambiarEstadoActualWs( $orderId );
                $requestDb = PickitUtilities::cambiarEstadoActualDb( $orderId );

                return $this->display(__FILE__, 'views/templates/hook/vistaadminorderdetail_exito.tpl');
            }

            return $this->display(__FILE__, 'views/templates/hook/vistaadminorderdetail.tpl');
        } elseif ( $estadoActualOrder == "Disponible para retiro") {
            return $this->display(__FILE__, 'views/templates/hook/vistaadminorderdetail_no.tpl');
        }
    }
}