{**
* 2007-2019 PrestaShop SA and Contributors
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to https://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2019 PrestaShop SA and Contributors
* @license https://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="col-lg-6">
    <div class="panel pickit-panel-etiqueta">
        <div class="panel-heading">
            <i class="icon-truck"></i> Pickit - Etiqueta de envío
        </div>
        <div id="url-etiqueta">
            <a class="btn btn-primary pull-right pickit-etiqueta-url" href="{$urlEtiqueta}" target='_blank'>Etiqueta</a>
            <p> Accede a la etiqueta del envío. </p>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="panel pickit-panel-action">
        <div class="panel-heading">
            <i class="icon-truck"></i> Pickit - Cambiar de estado
        </div>
        <div id="action">
            <p> No es posible cambiar el estado de la orden, ya se encuentra en "Disponible para retiro".</p>
        </div>
    </div>
</div>