<?php
/**
 * 2007-2019 PrestaShop SA and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pickit_global` (
    `pickit_id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
    `pickit_testing_mode` BOOLEAN,
    `pickit_apikey_webapp` char(255),
    `pickit_token_id` char(255),
    `pickit_apikey_webapp_test` char(255),
    `pickit_token_id_test` char(255),
    `pickit_pais_tienda` tinyint(1),
    `pickit_url_webservice` char(255),
    `pickit_url_webservice_test` char(255),
    `pickit_product_weight` tinyint(1),
    `pickit_product_dim` tinyint(1),
    `pickit_imposition_available` tinyint(1),
    `pickit_estado_inicial` tinyint(1),
    /*
    `pickit_ship_type` tinyint(1),
    `pickit_ship_price_opt_dom` tinyint(1),
    `pickit_ship_price_fijo_dom` int(25),
    `pickit_ship_price_porcentual_dom` float(25),
    */
    `pickit_ship_price_opt_punto` tinyint(1),
    `pickit_ship_price_fijo_punto` int(25),
    `pickit_ship_price_porcentual_punto` float(25),
    `pickit_ship_price_plus_punto` float(25),
    `pickit_servicio_drop_off` BOOLEAN,
    PRIMARY KEY  (`pickit_id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'pickit_order` (
    pickit_id int(9) NOT NULL AUTO_INCREMENT,
    pickit_order_id int(9),
    pickit_transaccion_id int(9),
    pickit_etiqueta mediumtext NULL,
    pickit_seguimiento mediumtext NULL,
    pickit_estado_actual char(50),
    pickit_orden_impuesta char(30),
    pickit_ambiente char(50),
    PRIMARY KEY  (pickit_id)
  ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
